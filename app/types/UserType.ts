export interface IUserInformation {
  name?: string
  email?: string
}
