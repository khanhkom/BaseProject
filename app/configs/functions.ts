import { Dimensions, Alert } from "react-native"
import { initialWindowMetrics } from "react-native-safe-area-context"
import moment from "moment"

const { width, height } = Dimensions.get("window")
const { width: widthScreen, height: heightScreen } = Dimensions.get("screen")
const deviceHeight = height - (initialWindowMetrics?.insets.top ?? 0)
export const responsiveHeight = (h: number): number => height * (h / 100)
export const WIDTH = (w: number): number => width * (w / 360)
export const HEIGHT = (h: number): number => deviceHeight * (h / 640)
export const WIDTH_SCREEN = (w: number): number => widthScreen * (w / 360)
export const HEIGHT_SCREEN = (h: number): number => heightScreen * (h / 640)
export const getWidth = (): number => width
export const getHeight = (): number => height
export const getLineHeight = (f: number): number => f
export const getHighAbsolute = (h: number): number => height * (h / 640)
export const getInsetVertical = (): number =>
  (initialWindowMetrics?.insets.top || 0) + (initialWindowMetrics?.insets.bottom || 0)

export const getFont = (f: number): number => {
  return RFValue(f)
}

export function RFPercentage(percent) {
  const heightPercent = (percent * (deviceHeight ?? 0)) / 100
  return Math.round(heightPercent)
}

export function RFValue(fontSize) {
  const heightPercent =
    (((initialWindowMetrics?.insets.top ?? 0) > 20 ? fontSize : fontSize - 2) *
      (deviceHeight ?? 0)) /
    getHeight()
  return Math.round(heightPercent)
}

export const FuntionAlert = (body: {
  notice: string
  content: string
  disableCancel?: boolean
  leftTitle?: string
  rightTitle?: string
  onPressCan?: () => void
  onPressOk?: () => void
}): void => {
  Alert.alert(
    body.notice,
    body.content,
    body.disableCancel
      ? [
          {
            text: body.rightTitle || "OK",
            onPress: () => {
              body.onPressOk && body.onPressOk()
            },
          },
        ]
      : [
          {
            text: body.leftTitle || "Cancel",
            onPress: () => {
              body.onPressCan && body.onPressCan()
            },
          },
          {
            text: body.rightTitle || "OK",
            onPress: () => {
              body.onPressOk && body.onPressOk()
            },
          },
        ],
    { cancelable: false },
  )
}

export const getFileExtintion = (filePath = ""): string | undefined => {
  return filePath?.split(".").pop()
}

export const isLocalFile = (fileUri) => {
  const subStr = fileUri?.substring(0, 5)
  if (subStr === "https" || !fileUri) {
    return false
  }
  return true
}

export const customFormatDate = (dateString = 0) => {
  return moment(dateString).format("D.M.YYYY")
}
