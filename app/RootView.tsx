import React, { Component } from "react"
import { StyleSheet, SafeAreaView } from "react-native"

import OneSignal from "react-native-onesignal"
import { ApplicationProvider } from "@ui-kitten/components"
import * as eva from "@eva-design/eva"
import AsyncStorageUtils from "./helpers/AsyncStorageUtils"
import env, { oneSignalKey } from "./env"
import { RootSiblingParent } from "react-native-root-siblings"
import NoInternetComponent from "./common/NoInternet"
// import env from "./env"
import R from "./assets/R"
// import NotificationFirebase from './helpers/NotificationFirebase'

class RootView extends Component {
  constructor(props) {
    super(props)
    this.state = {}

    OneSignal.setAppId(oneSignalKey.development)
    OneSignal.setLogLevel(6, 0)
    OneSignal.setNotificationWillShowInForegroundHandler((notifReceivedEvent) => {
      console.log("OneSignal: notification will show in foreground:", notifReceivedEvent)
      const notif = notifReceivedEvent.getNotification()
      this.onReceived(notif)
    })
    OneSignal.setNotificationOpenedHandler((notification) => {
      this.onOpened(notification)
    })
  }

  onReceived = (notification) => {
    console.log("notification_notification", notification)
  }

  onOpened = async ({ notification, action }) => {
    console.log("onOpened_noti", notification, action)
  }

  componentDidMount() {
    this.onIds()
  }

  onIds = async () => {
    const deviceState = await OneSignal.getDeviceState()
    AsyncStorageUtils.save(AsyncStorageUtils.KEY.SAVE_TOKEN_NOTI, deviceState.userId)
  }

  render() {
    return (
      <>
        <ApplicationProvider {...eva} theme={eva.light}>
          <SafeAreaView style={styles.container}>
            <RootSiblingParent>
              {this.props.children}
              <NoInternetComponent />
              {/* <VersionChecker /> */}
            </RootSiblingParent>
          </SafeAreaView>
        </ApplicationProvider>
      </>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: R.colors.white0,
    flex: 1,
  },
})

export default RootView
