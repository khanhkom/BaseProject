import APISauce, { ApiResponse } from "apisauce"
import { handleErrorApi } from "./handleError"
import URL from "./url"

export type IRequest<P> = {
  endpoint: string
  params?: P
  showToast?: boolean
}

export type IResponse<R> = ApiResponse<R> & {
  showToast?: boolean
}

export const rootServerInstance = APISauce.create({
  baseURL: URL.ROOT_API,
})

export const s3ServerInstance = APISauce.create({
  baseURL: URL.ROOT_API,
})

const formdataConfig = {
  headers: {
    "Content-Type": "multipart/form-data",
  },
}

export async function getData<P, R>(request: IRequest<P>): Promise<IResponse<R>> {
  return await rootServerInstance.get<R>(request.endpoint, request.params).then((response) => {
    return handleErrorApi<R>(response, request.showToast)
  })
}
export async function postData<P, R>(request: IRequest<P>): Promise<IResponse<R>> {
  return await rootServerInstance.post<R>(request.endpoint, request.params).then((response) => {
    return handleErrorApi<R>(response, request.showToast)
  })
}
export async function putData<P, R>(request: IRequest<P>): Promise<IResponse<R>> {
  return await rootServerInstance.put<R>(request.endpoint, request.params).then((response) => {
    return handleErrorApi<R>(response, request.showToast)
  })
}
export async function delData<P, R>(request: IRequest<P>): Promise<IResponse<R>> {
  return await rootServerInstance.delete<R>(request.endpoint, request.params).then((response) => {
    return handleErrorApi<R>(response, request.showToast)
  })
}

export async function postFormData<P, R>(request: IRequest<P>): Promise<IResponse<R>> {
  return await rootServerInstance
    .post<R>(request.endpoint, request.params, formdataConfig)
    .then((response) => {
      return handleErrorApi<R>(response, request.showToast)
    })
}
export async function postUploadFile<P, R>(request: IRequest<P>): Promise<IResponse<R>> {
  return await s3ServerInstance
    .post<R>(request.endpoint, request.params, formdataConfig)
    .then((response) => {
      return handleErrorApi<R>(response, request.showToast)
    })
}
