const ROOT = "https://dev.app.cleverday.com"
// const ROOTMOCK = "http://192.168.43.117:4010";
const ROOT_API = ROOT
const URL = {
  ROOT_API,
  GET_HOWTO_BY_ID: "/howtos/",
}
export default URL
