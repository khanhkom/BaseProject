import Toast from "react-native-root-toast"
import STATUS from "./status"
import { IResponse } from "./helpers"

import { ApiResponse } from "apisauce"

type IResponseError = IResponse<unknown> & {
  customMessage?: string
}

export const showMessageError = (responseError: Partial<IResponseError>) => {
  const toast = Toast.show(
    responseError.customMessage || responseError.problem || "Network Error",
    {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    },
  )
  setTimeout(function () {
    Toast.hide(toast)
  }, 1000)
}

function handleData<R>(responseError: IResponse<R>, showToast?, message?): IResponse<R> {
  if (showToast) {
    showMessageError({ ...responseError, customMessage: message })
  }
  return responseError
}

export function handleErrorApi<R>(response: ApiResponse<R>, showToast?): ApiResponse<R> {
  switch (response.status) {
    case STATUS.FAILED:
      return handleData(response, showToast, "Bad request")
    case STATUS.TOKEN_EXPIRED:
      return handleData(response, showToast, "Token expired")
    case STATUS.FORBIDDEN:
      return handleData(response, showToast, "Forbidden")
    case STATUS.NOT_FOUND:
      return handleData(response, showToast, "Page not found")
    case STATUS.UNAUTHORIZED:
      return handleData(response, showToast, "Unauthorized")
  }
  return handleData<R>(response, showToast)
}

export const checkStatus = (responseStatus, checkingStatus = STATUS.SUCCESS) =>
  responseStatus === checkingStatus
