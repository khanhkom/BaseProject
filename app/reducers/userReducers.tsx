import { IUserInformation } from "../types"
import { USER_ACTION } from "../actions/actionTypes"
export type uploadAction = {
  type: string
  user: IUserInformation
}

const defaultState: IUserInformation = {
  name: "name",
  email: "email",
}

export default (stateUpload = defaultState, action: uploadAction): IUserInformation => {
  switch (action.type) {
    case USER_ACTION.USER_FETCH_SUCCEEDED: {
      return action.user
    }
    default:
      return stateUpload
  }
}
