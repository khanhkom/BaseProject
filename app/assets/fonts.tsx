const fonts = {
  Roboto: "Roboto-Regular",
  RobotoMedium: "Roboto-Medium",
  RobotoBold: "Roboto-Bold",
}
export default fonts
