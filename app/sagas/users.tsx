import { put, takeEvery } from "redux-saga/effects"
import { USER_ACTION } from "../actions/actionTypes"
// worker Saga: will be fired on USER_FETCH_REQUESTED actions
function* fetchUser() {
  try {
    const user = {}
    yield put({ type: USER_ACTION.USER_FETCH_SUCCEEDED, user: user })
  } catch (e) {
    yield put({ type: USER_ACTION.USER_FETCH_FAILED, message: e.message })
  }
}

function* getUserInfor() {
  yield takeEvery(USER_ACTION.USER_FETCH_REQUESTED, fetchUser)
}

export default getUserInfor
