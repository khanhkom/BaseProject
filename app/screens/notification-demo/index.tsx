import React from "react"
import { View, Text } from "react-native"
import styles from "./styles"
import { Button, Screen, Wallpaper } from "../../components"
import { Popup, showToast, TypeToast, showAlert } from "@common/Notification"

type Props = {}

export const NotificationDemo: React.FC<Props> = (props: Props) => {
  const [popupVissible, setPopupVisisble] = React.useState(false)
  return (
    <View style={styles.container}>
      <Wallpaper />
      <Popup
        visible={popupVissible}
        title="This is title"
        message="This is content. But content is optional, it can be null."
        leftText="Cancel"
        rightText="Ok"
        leftButton={() => setPopupVisisble(false)}
        rightButton={() => setPopupVisisble(false)}
      />
      <Screen style={styles.screen} preset="scroll">
        <Button
          style={styles.button}
          tx="Notification.showpopup"
          onPress={() => setPopupVisisble(true)}
          textStyle={styles.text}
        />
        <Button
          style={styles.button}
          tx="Notification.warn"
          onPress={() =>
            showToast({
              title: "This is title",
              message: "Warnning toast",
              type: TypeToast.WARNING,
            })
          }
          textStyle={styles.text}
        />
        <Button
          style={styles.button}
          tx="Notification.success"
          onPress={() =>
            showToast({ title: "This is title", message: "Success toast", type: TypeToast.SUCCESS })
          }
          textStyle={styles.text}
        />
        <Button
          style={styles.button}
          tx="Notification.error"
          onPress={() =>
            showToast({ title: "This is title", message: "Error toast", type: TypeToast.ERROR })
          }
          textStyle={styles.text}
        />
        <Button
          style={styles.button}
          tx="Notification.alert"
          onPress={() =>
            showAlert({
              title: "This is title alert",
              message: "This is message alert",
              disableCancel: true,
            })
          }
          textStyle={styles.text}
        />
      </Screen>
    </View>
  )
}
