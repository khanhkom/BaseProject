import { StyleSheet } from "react-native"
import { spacing } from "./../../theme/spacing"
import R from "../../assets/R"

const styles = StyleSheet.create({
  button: {
    backgroundColor: R.colors.purpleA800,
    marginVertical: spacing[4],
    paddingHorizontal: spacing[4],
    paddingVertical: spacing[4],
  },
  container: { flex: 1 },
  screen: {
    backgroundColor: R.colors.purple500,
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: spacing[4],
  },
  text: {
    color: R.colors.white100,
    fontSize: 16,
    fontWeight: "bold",
  },
})

export default styles
