import React from "react"
import Toast, { ToastOptions } from "react-native-root-toast"
import { View, Text, StyleSheet } from "react-native"
import R from "../../../assets/R"
import { WIDTH } from "../../../configs/functions"
import { HEIGHT } from "./../../../configs/functions"
import FastImage from "react-native-fast-image"

type ToastConfig = {
  title: string
  message?: string
  option?: ToastOptions
  type?: TypeToast
}

export enum TypeToast {
  WARNING = "WARNING",
  SUCCESS = "SUCCESS",
  ERROR = "ERROR",
}

const image = {
  warn: require("./assests/Warning.png"),
  error: require("./assests/Error.png"),
  success: require("./assests/Success.png"),
}

const getColor = (type: TypeToast) => {
  switch (type) {
    case TypeToast.WARNING:
      return R.colors.yellowd200
    case TypeToast.ERROR:
      return R.colors.redab
    default:
      return R.colors.green56
  }
}

const getImage = (type: TypeToast) => {
  switch (type) {
    case TypeToast.WARNING:
      return image.warn
    case TypeToast.ERROR:
      return image.error
    default:
      return image.success
  }
}
/**
 * this function tho show toast, you can show with 3 type (error, success, warnning)
 */
export const showToast = (toastConfig: ToastConfig) => {
  const ToastComponent = (
    <View style={styles.container}>
      <View style={[styles.bage, { backgroundColor: getColor(toastConfig.type) }]} />
      <View style={styles.content}>
        <View>
          <Text style={[styles.title, { color: getColor(toastConfig.type) }]}>
            {toastConfig.title}
          </Text>
          {toastConfig.message && <Text>{toastConfig.message}</Text>}
        </View>
        <FastImage source={getImage(toastConfig.type)} style={styles.img} resizeMode="contain" />
      </View>
    </View>
  )
  Toast.show(
    (ToastComponent as unknown) as string, // toast not support type script, use this trick to bypass
    {
      duration: Toast.durations.SHORT,
      position: Toast.positions.BOTTOM,
      shadow: false,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: "transparent",
      opacity: 1,
      ...toastConfig.option,
    },
  )
}

const styles = StyleSheet.create({
  bage: {
    alignSelf: "center",
    height: 5,
    width: WIDTH(320),
  },
  container: {
    backgroundColor: R.colors.white100,
    borderBottomLeftRadius: WIDTH(5),
    borderBottomRightRadius: WIDTH(5),
    elevation: 3,
    flex: 1,
    minHeight: HEIGHT(80),
    paddingHorizontal: WIDTH(15),
    shadowColor: R.colors.black0,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    width: WIDTH(320),
  },
  content: { flexDirection: "row", justifyContent: "space-between" },
  img: {
    height: HEIGHT(70),
    width: WIDTH(70),
  },
  title: {
    color: R.colors.green600,
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: HEIGHT(6),
  },
})
