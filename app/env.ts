import { Platform } from "react-native"
let NODE_DEV = "development"

// eslint-disable-next-line no-undef
if (__DEV__) {
  NODE_DEV = "production"
} else {
  NODE_DEV = "production"
}

const serverURL = {
  // development: 'http://192.168.1.88:3000', // ip company
  development: "http://192.168.1.177:3000", // ip company
  production: "http://165.22.108.71:3000",
}

const socketURL = {
  development: "http://165.22.108.71:8888", // ip company
  production: "http://165.22.108.71:8888",
}

const codePushKey = {
  development: Platform.OS === "ios" ? "ios-key-development" : "android-key-development",
  production: Platform.OS === "ios" ? "ios-key-development" : "android-key-development",
}

export const oneSignalKey = {
  development: "e70d0dfe-31b2-490d-b817-1d83dc594d2c",
  production: "e70d0dfe-31b2-490d-b817-1d83dc594d2c",
}

const SENTRY_KEY = "sentry-key-here"

export default {
  currentNode: NODE_DEV,
  serverURL: serverURL[NODE_DEV],
  socketURL: socketURL[NODE_DEV],
  codePushKey: codePushKey[NODE_DEV],
  oneSignalKey: oneSignalKey[NODE_DEV],
  sentryKey: SENTRY_KEY,
}
